package main

import (
	"flag"
	"server"
	"client"
	"log"
	"time"
	"os"
	"path"
)

var (
	isServer bool
	networkAddr string
	networkTimeOut int
)

func init() {
	flag.BoolVar(&isServer, "server", false, "add flag if you wan't to be host for other client")
	flag.StringVar(&networkAddr, "addr", "127.0.0.1:9090", "network addres to connect or listen")
	flag.IntVar(&networkTimeOut, "timeout", 10, "network timeout for read and write")
	flag.Parse()
}

func main() {
	if isServer {
		serv := &server.Server{
			ClientTimeOut: time.Second * time.Duration(networkTimeOut),
			FileSavePath: path.Join(os.TempDir(), "chatServer"),
		}
		serv.Run(networkAddr)
	} else {
		cli := &client.Client{}
		cli.Run(networkAddr)
	}


	log.Println("Complete")
}
