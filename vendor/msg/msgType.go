package msg

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"net"
	"os"
	"strconv"
	"time"
)

const (
	_ byte = iota
	TEXT_MSG
	FILE_MSG
	PING_MSG
)

type MessageJSON struct {
	Type string `json:"type"`
	Data string `json:"msg"`
}

func (m MessageJSON) ToMessage() ([]byte, error) {
	out, err := json.Marshal(m)
	if err != nil {
		return out, err
	}

	return prepareMsg(TEXT_MSG, out).Bytes(), nil
}

func (m *MessageJSON) ParseMess(data []byte) error {
	return json.Unmarshal(data, &m)
}

func prepareMsg(msgType byte, data []byte) *bytes.Buffer {
	buf := bytes.NewBuffer([]byte{msgType})
	length := IntToByte(len(data))
	buf.WriteByte(4)
	buf.Write(length)
	buf.Write(data)

	return buf
}

func CreateTextMessageFromString(mess string) []byte {
	buf := prepareMsg(TEXT_MSG, []byte(mess))

	return buf.Bytes()
}

func ReadTextMessage(c net.Conn) ([]byte, error) {
	var (
		totalRead int
		out       = make([]byte, 0)
	)

	length := []byte{0}
	_, err := c.Read(length)
	if err != nil {
		return out, err
	}

	messLength := make([]byte, int(length[0]))
	c.Read(messLength)

	bodyLength := ByteToInt(messLength)
	for totalRead < bodyLength {
		var needToRead = 4096
		if totalRead+needToRead > bodyLength {
			needToRead = bodyLength - totalRead
		}
		tmpMsg := make([]byte, needToRead)
		n, err := c.Read(tmpMsg)
		if err != nil {
			return out, err
		}

		totalRead += n
		out = append(out, tmpMsg...)
	}

	return out, err
}

func SaveFileFromMessage(c net.Conn, path string) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()

	length := []byte{0}
	_, err = c.Read(length)
	if err != nil {
		return err
	}
	messLength := make([]byte, int(length[0]))

	c.Read(messLength)

	bodyLength := ByteToInt(messLength)
	totalRead := 0

	for totalRead < bodyLength {
		c.SetReadDeadline(time.Now().Add(time.Second * 5))
		var needToRead = 4096
		if totalRead+needToRead > bodyLength {
			needToRead = bodyLength - totalRead
		}
		tmpMsg := make([]byte, needToRead)
		n, err := c.Read(tmpMsg)
		if err != nil {
			return err
		}
		totalRead += n
		file.Write(tmpMsg)
	}

	return nil
}

func ByteToInt(in []byte) int {
	i, err := strconv.ParseInt(fmt.Sprintf("%x", in), 16, 64)
	if err != nil {
		return -1
	}

	return int(i)
}

func IntToByte(in int) []byte {
	t := make([]byte, 4)
	binary.LittleEndian.PutUint32(t, uint32(in))

	var j = 0
	for i := 3; i > j; i-- {
		first, last := t[j], t[i]
		t[j] = last
		t[i] = first

		j++
	}

	return t
}
