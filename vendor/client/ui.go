package client

import (
	"github.com/marcusolsson/tui-go"
	"time"
)

var posts = tui.NewList()

func addPost(postString string) {
	if posts.Length() > 20 {
		posts.RemoveItem(0)
	}

	posts.AddItems(postString)
}

func CreateUi(inMsg, outMsg chan []byte) {
	history := tui.NewVBox()
	history.SetBorder(true)
	history.Append(tui.NewSpacer())
	history.Append(posts)

	input := tui.NewEntry()
	input.SetFocused(true)
	input.SetSizePolicy(tui.Expanding, tui.Maximum)

	inputBox := tui.NewHBox(input)
	inputBox.SetBorder(true)
	inputBox.SetSizePolicy(tui.Expanding, tui.Maximum)

	chat := tui.NewVBox(history, inputBox)
	chat.SetSizePolicy(tui.Expanding, tui.Expanding)

	input.OnSubmit(func(e *tui.Entry) {
		mess := []byte(e.Text())
		if len(mess) == 0 {
			return
		}
		outMsg <- mess
		input.SetText("")
	})

	root := tui.NewHBox(chat)
	ui := tui.New(root)

	go func() {
		for msg := range inMsg {
			addPost(string(msg))
		}
		ui.Quit()
	}()

	go func() {
		for {
			ui.Update(func() {})
			time.Sleep(time.Millisecond * 300)
		}
	}()

	ui.SetKeybinding("Esc", func() { ui.Quit() })
	if err := ui.Run(); err != nil {
		panic(err)
	}
}
