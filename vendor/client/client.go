package client

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"msg"
	"net"
	"os"
	"strings"
	"time"
)

const GB = 1024 * 1024 * 1024

type Client struct {
	Conn     net.Conn
	NickName string
	IsActive bool

	SendCh         chan []byte
	ReciveCh       chan []byte
	LastFileFormat string
}

func (c *Client) Run(addr string) {
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		log.Println("can't connect to server", addr, err)
		return
	}

	c.Conn = conn
	c.SendCh = make(chan []byte, 10)
	c.ReciveCh = make(chan []byte, 10)
	c.IsActive = true

	go c.ReadLoop()
	go c.WriteLoop()

	fmt.Println("Hello there! Please enter you nickname for chat:")
	fmt.Scanln(&c.NickName)
	fmt.Println("Okay, your nick is", c.NickName, "now")

	msg, _ := msg.MessageJSON{
		Type: "reg",
		Data: c.NickName,
	}.ToMessage()

	c.Conn.Write(msg)

	CreateUi(c.ReciveCh, c.SendCh)
}

func (c *Client) ReadLoop() {
	for c.IsActive {
		mess, err := c.ReadMessage()
		if err != nil {
			log.Println("can't read message from server", err)
			break
		}

		if len(mess) == 0 {
			continue
		}

		c.ReciveCh <- mess

	}
	close(c.ReciveCh)
	c.IsActive = false
}

func (c *Client) WriteLoop() {
	pinger := time.NewTicker(time.Second * 4)
	for c.IsActive {
		select {
		case mess := <-c.SendCh:
			if strings.HasPrefix(string(mess), "/file") {
				c.ReciveCh <- []byte("Start file send")
				filePath := strings.Replace(string(mess), "/file", "", 1)
				c.sendFile(filePath)
				continue
			}
			c.MakeTextMsg(mess)

		case <-pinger.C:
			c.Conn.SetDeadline(time.Now().Add(time.Second * 5))
			_, err := c.Conn.Write([]byte{msg.PING_MSG})
			if err != nil {
				c.IsActive = false
			}
		}
	}
}

func (c *Client) sendFile(file string) {
	file = strings.TrimSpace(file)
	f, err := os.Open(file)
	if err != nil {
		c.ReciveCh <- []byte("error on " + err.Error())
		return
	}
	defer f.Close()
	stat, _ := f.Stat()

	formatIndex := strings.LastIndex(f.Name(), ".")
	if formatIndex < 0 {
		c.ReciveCh <- []byte("can't detect file format")
		return
	}

	if stat.Size() > GB {
		c.ReciveCh <- []byte("file is to large. Max 1GB")
		return
	}

	m, _ := msg.MessageJSON{Type: "file", Data: f.Name()[formatIndex:]}.ToMessage()
	c.Conn.Write(m)

	//Init file loader
	b := []byte{msg.FILE_MSG, 4}

	size := int(stat.Size())
	b = append(b, msg.IntToByte(size)...)
	c.Conn.Write(b)

	buf := bufio.NewReader(f)

	n := 0
	for {
		emptySlice := make([]byte, 4096)
		n, err = buf.Read(emptySlice)
		if err != nil {
			break
		}
		c.Conn.Write(emptySlice[:n])
	}
	if err != io.EOF {
		c.ReciveCh <- []byte(err.Error())
	}

	c.ReciveCh <- []byte("file read compleate ")
}

func (c *Client) MakeTextMsg(mess []byte) error {
	m, _ := msg.MessageJSON{
		Type: "msg",
		Data: string(mess),
	}.ToMessage()

	_, err := c.Conn.Write(m)

	return err
}

func (c *Client) ReadMessage() ([]byte, error) {
	//Skip firt byte. Client receive only text msg
	s := []byte{0}
	c.Conn.Read(s)

	buf, err := msg.ReadTextMessage(c.Conn)
	if err != nil {
		fmt.Println("can't read msg from server", err)
	}

	return buf, err
}
