package server

import (
	"client"
	"fmt"
	"log"
	"msg"
	"net"
	"os"
	"sync"
	"time"
)

type Server struct {
	Listener net.Listener
	Clients  []*client.Client

	MsgCh         chan []byte
	FileSavePath  string
	fileCount     int64
	ClientTimeOut time.Duration
	m             *sync.RWMutex
}

func (s *Server) Run(addr string) {
	ln, err := net.Listen("tcp", addr)
	if err != nil {
		log.Println("can't start server on", addr, err)
		return
	}
	s.Listener = ln

	s.checkSavePathDir()

	s.Clients = make([]*client.Client, 0)
	s.m = &sync.RWMutex{}
	s.MsgCh = make(chan []byte, 100)

	log.Println("Server start on", addr, "wait for cleint..")

	go s.AcceptClients()
	s.BroadcastMessages()
}

func (s *Server) AcceptClients() {
	for {
		conn, err := s.Listener.Accept()
		if err != nil {
			log.Println("can't accept connect", err)
		}

		log.Println("New client", conn.RemoteAddr())

		cli := &client.Client{
			Conn: conn,
		}

		s.m.Lock()
		s.Clients = append(s.Clients, cli)
		s.m.Unlock()

		go s.ProcessClientMessage(cli)

	}
}

func (s *Server) DeleteClient(c *client.Client) {
	defer c.Conn.Close()

	var index int = -1

	//TODO rewrite to map
	s.m.RLock()
	for i, client := range s.Clients {
		if client == c {
			index = i
			break
		}
	}
	s.m.RUnlock()

	if index < 0 {
		log.Println("can't find client", c)
		return
	}

	s.m.Lock()
	s.Clients = append(s.Clients[:index], s.Clients[index+1:]...)
	s.m.Unlock()
}

func (s *Server) ProcessClientMessage(c *client.Client) {
	defer s.DeleteClient(c)

	for {
		c.Conn.SetReadDeadline(time.Now().Add(s.ClientTimeOut))
		msgType := []byte{0}
		_, err := c.Conn.Read(msgType)
		if err != nil {
			log.Println("can't read from client", err)
			break
		}

		switch msgType[0] {
		case msg.PING_MSG:
			c.Conn.SetDeadline(time.Now().Add(s.ClientTimeOut))
		case msg.TEXT_MSG:
			mess, err := msg.ReadTextMessage(c.Conn)
			if err != nil {
				log.Println("can't read from client", err)
			}
			m := msg.MessageJSON{}
			m.ParseMess(mess)

			if m.Type == "reg" {
				c.NickName = m.Data
				continue
			}

			if m.Type == "msg" {
				st := fmt.Sprintf("%s <%s> %s", time.Now().Format("15:04:05"), c.NickName, m.Data)
				s.MsgCh <- []byte(st)
				continue
			}

			if m.Type == "file" {
				c.LastFileFormat = m.Data
				continue
			}

		case msg.FILE_MSG:
			s.m.Lock()
			s.fileCount++
			s.m.Unlock()

			format := c.LastFileFormat
			savePath := fmt.Sprintf("%s/%s_%d%s", s.FileSavePath, c.NickName, s.fileCount, format)
			err = msg.SaveFileFromMessage(c.Conn, savePath)
			if err != nil {
				log.Println("can't save file from client", err, savePath)
				continue
			}
			c.LastFileFormat = ""
			log.Println("Saved file from", c.NickName, "to", savePath)

		default:
			log.Println("Wrong message type. Delete client", c.Conn)
			break
		}

	}
}

func (s *Server) sendTextMessageToClient(c *client.Client, message []byte) {
	var err error
	defer func() {
		if err != nil {
			s.DeleteClient(c)
		}
	}()

	outMsg := msg.CreateTextMessageFromString(string(message))

	c.Conn.SetWriteDeadline(time.Now().Add(s.ClientTimeOut))
	_, err = c.Conn.Write(outMsg)
	if err != nil {
		fmt.Println("can't send message to client", err)
		return
	}
}

func (s *Server) BroadcastMessages() {
	for {
		message := <-s.MsgCh
		log.Println("Sending to clients", string(message))

		s.m.RLock()
		for _, c := range s.Clients {
			go s.sendTextMessageToClient(c, message)
		}
		s.m.RUnlock()
	}
}

func (s Server) checkSavePathDir() {
	_, err := os.Open(s.FileSavePath)
	if os.IsNotExist(err) {
		os.MkdirAll(s.FileSavePath, 0777)
		log.Println("Create dir", s.FileSavePath)
	}
}
